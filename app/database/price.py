from flask_appbuilder import Model
from sqlalchemy import Column, Integer, ForeignKey, DATE, Float
from sqlalchemy.orm import relationship, backref

from .stock_item import StockItem
from .supermarket import Supermarket


####################
# Pricing Model
####################

class Pricing(Model):
    """
    This class represents a table in Stock Tracking database, which stores information
    of price of a single item
    """
    __tablename__ = 'price'
    id = Column(Integer, primary_key=True)
    price = Column(Float, nullable=False)
    date_of_purchase = Column(DATE, nullable=False)
    stock_item_id = Column(Integer, ForeignKey(f'{StockItem.__tablename__}.id'), nullable=False)
    stock_item = relationship(StockItem.__name__, backref=backref('price_history', passive_deletes=False))
    supermarket_id = Column(Integer, ForeignKey(f'{Supermarket.__tablename__}.id'), nullable=False)
    supermarket = relationship(Supermarket.__name__, backref=backref('price_list', passive_deletes=False))

    def __repr__(self):
        return f'{self.stock_item.item_name} price'

from flask_appbuilder import Model
from sqlalchemy import Column, Integer, DateTime, String, ForeignKey, Float
from sqlalchemy.orm import relationship

from .shopping_list import shopping_list
from .supermarket import Supermarket
from .database_constants import DatabaseConstants
from .utils import get_database_session


####################
# Shopping Plan Model
####################

class ShoppingPlan(Model):
    """
    This class represents the table in Stock Tracking database,
    which stores the information of a shopping plan
    """
    __tablename__ = 'shopping_plan'
    id = Column(Integer, primary_key=True)
    supermarket_id = Column(Integer, ForeignKey(f'{Supermarket.__tablename__}.id'), nullable=False)
    supermarket = relationship(Supermarket.__name__)
    plan_shopping_date = Column(DateTime, nullable=False)
    plan_completion_date = Column(DateTime, nullable=True)
    status = Column(String(20), nullable=False)
    total_cost = Column(Float, nullable=True)
    stock_item_list = relationship(
        'StockItem', secondary=shopping_list, back_populates='shopping_plan_list', cascade='all',
        passive_deletes=True
    )

    def __repr__(self):
        return self.plan_shopping_date

    def complete_shopping_plan(self):
        self.status = DatabaseConstants.COMPLETE

    @staticmethod
    def get_ongoing_shopping_plan():
        """
        this static method will return a list of Shopping Plan objects whose status is 'Ongoing'
        :return: list of Shopping Plan objects
        """
        session = get_database_session()
        return session.query(ShoppingPlan).filter_by(status='Ongoing').one_or_none()

    @staticmethod
    def get_shopping_plan_by_id(shopping_plan_id: int):
        """
        This static method will return a Shopping Plan object given the shopping plan id
        :param shopping_plan_id: The id of a Shopping Plan
        :return: Shopping Plan object
        """
        session = get_database_session()
        return session.query(ShoppingPlan).filter_by(id=shopping_plan_id).one_or_none()

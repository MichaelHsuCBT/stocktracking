from flask_appbuilder import Model
from sqlalchemy import Column, Integer, ForeignKey, DateTime, Float
from sqlalchemy.orm import relationship, backref

from .stock_item import StockItem
from .supermarket import Supermarket


###############################
# History Model
###############################

class ItemHistory(Model):
    """
    This class represents a table in this Stock Tracking database, which
    stores the item history data
    """
    __tablename__ = 'item_history'
    id = Column(Integer, primary_key=True)
    item_id = Column(Integer, ForeignKey(f'{StockItem.__tablename__}.id'), nullable=False)
    item = relationship('StockItem', backref=backref('history', uselist=False))
    supermarket_id = Column(Integer, ForeignKey(f'{Supermarket.__tablename__}.id'), nullable=False)
    supermarket = relationship('Supermarket', backref=backref('history', uselist=False))
    date = Column(DateTime, nullable=False)
    unit_price = Column(Float, nullable=False)

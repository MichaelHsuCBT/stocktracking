def get_database_session():
    """
    This method returns the current thread-local database session. It's designed to allow
    queries to be performed using the session without hacing ro expose the "db" object externally.
    :return:
    """

    from app import db
    return db.session


def add_object_to_database(obj) -> None:
    """
    This function adds a Model object to the current SQLAlchemy session, which will cause it
    to be inserted into the database.
    :param obj: Object to be inserted
    :return: None
    """
    get_database_session().add(obj)


def commit_database_transaction() -> None:
    """
    This function commits the current database transaction, which will cause all adds, edits
    and deletions to be merged into the database and become cisible to other users.
    :return:
    """
    get_database_session().commit()

from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, Float
from .database_constants import DatabaseConstants

from .utils import get_database_session


###############################
# Supermarket Model
###############################

class Supermarket(Model):
    """
    This class represents a table in this Stock Tracking database, which
    stores the supermarket information
    """
    __tablename__ = 'supermarket'
    id = Column(Integer, primary_key=True)
    supermarket_name = Column(String(DatabaseConstants.supermarket_name_len), nullable=False)
    location = Column(String(DatabaseConstants.location_len), nullable=False)
    distance = Column(Float, nullable=False)
    total_visits = Column(Integer, nullable=True)

    def __repr__(self):
        return self.supermarket_name

    @staticmethod
    def list_supermarkets() -> list:
        session = get_database_session()
        return session.query(Supermarket).all()

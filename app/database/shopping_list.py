from flask_appbuilder import Model
from sqlalchemy import Column, Integer, ForeignKey, Index, PrimaryKeyConstraint
from sqlalchemy import Table
from .utils import get_database_session

##############################
# ShoppingPlanStockItem Join Table
##############################

# This table joins together the columns in shopping_plan and stock_item with a many-to-many relationshop.
# Connect both Shopping Plan and Stock Item through the composite primary keys, not visible through GUI
# Having Index allows the keys to be ordered by Shopping Plan to Stock Item.
shopping_list = Table('shopping_list', Model.metadata,
                      Column('shopping_plan_id', Integer,
                             ForeignKey('shopping_plan.id', name='fk_shoppinglist_shoppingplan',
                                        ondelete="CASCADE"), nullable=False),
                      Column('stock_item_id', Integer,
                             ForeignKey('stock_item.id', name='fk_shoppinglist_stockitem',
                                        ondelete="CASCADE"), nullable=False),
                      PrimaryKeyConstraint('stock_item_id', 'shopping_plan_id', name='stock_item_shopping_plan_pk'),
                      Index('ix_shoppinglist_shoppingplanid_stockitemid', 'shopping_plan_id', 'stock_item_id',
                            unique=True),
                      Index('ix_shoppinglist_stockitemid_shoppingplanid', 'stock_item_id', 'shopping_plan_id',
                            unique=True)
                      )


def delete_shopping_plan_relationships_given_shopping_plan_id(shopping_plan_id: int) -> None:
    """
    The ON DELETE CASCADE option is not working in shopping_list many-to-many relationship table.
    So this function will manually delete entries in shopping_list table, which contains the
    given shopping_plan_id
    :param shopping_plan_id: The ID of shopping plan which will be deleted
    :return:
    """
    get_database_session().query(shopping_list).filter_by(shopping_plan_id=shopping_plan_id).\
        delete(synchronize_session=False)


def delete_shopping_plan_relationships_given_stock_item_id(stock_item_id: int) -> None:
    """
    The ON DELETE CASCADE option is not working in shopping_list many-to-many relationship table.
    So this function will manually delete entries in shopping_list table, which contains the
    given stock_item_id
    :param stock_item_id: The ID of stock items which will be deleted
    :return:
    """
    get_database_session().query(shopping_list).filter_by(stock_item_id=stock_item_id).\
        delete(synchronize_session=False)

from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String, ForeignKey, or_, and_
from sqlalchemy.orm import relationship, backref

from .category import Category
from .database_constants import DatabaseConstants
from .shopping_list import shopping_list
from .utils import get_database_session


####################
# Stock Item Model
####################

class StockItem(Model):
    """
    This class represents a table in the Stock Tracking database, which stores information
    of a single 'Stock Item'.
    """
    __tablename__ = 'stock_item'
    id = Column(Integer, primary_key=True)
    item_name = Column(String(DatabaseConstants.stock_item_name_len), nullable=False)
    category_id = Column(Integer, ForeignKey(f'{Category.__tablename__}.id'), nullable=False)
    category = relationship(Category.__name__, backref=backref('stock_item_list', passive_deletes=False))
    unit = Column(String(DatabaseConstants.stock_unit_len), nullable=True)
    percentage = Column(Integer, nullable=False)
    frequency = Column(String(DatabaseConstants.stock_frequency_len), nullable=False)
    shopping_plan_list = relationship(
        'ShoppingPlan', secondary=shopping_list, back_populates='stock_item_list', cascade='all',
        passive_deletes=True
    )

    def __repr__(self):
        return self.item_name

    @staticmethod
    def get_all_stock_items() -> list:
        """
        This method will return a list of all the Stock Items
        :return:
        """
        session = get_database_session()
        return session.query(StockItem).all()

    @staticmethod
    def get_all_lower_stock_items() -> list:
        """
        This static method will query the StockItem in database, whose count is less or equal than 1
        :return: a list of StockItem
        """
        session = get_database_session()
        return session.query(StockItem).filter(
            or_(
                and_(StockItem.frequency == DatabaseConstants.IMMEDIATELY, StockItem.percentage <= 20),
                and_(StockItem.frequency == DatabaseConstants.OFTEN, StockItem.percentage <= 5)
            )
        ).all()

    @property
    def category_name(self) -> str:
        """
        Show the category name through the stock item
        :return: the category name
        """
        return self.category.category_name

    @staticmethod
    def get_multiple_items_by_id_list(item_id_list: list) -> list:
        """
        This method will return a list of stock items given the items' id
        :param item_id_list: The list of stock item id
        :return: a list of StockItem object
        """
        session = get_database_session()
        return session.query(StockItem).filter(StockItem.id.in_(item_id_list)).all()

from flask_appbuilder import Model
from sqlalchemy import Column, Integer, String

from .database_constants import DatabaseConstants
from .utils import get_database_session


###############################
# Category Model
###############################

class Category(Model):
    """
    This class represents a table in this Stock Tracking database, which
    stores the category of each 'Stock Item' belonging to.
    """
    __tablename__ = 'category'
    id = Column(Integer, primary_key=True)
    category_name = Column(String(DatabaseConstants.category_name_len), unique=True, nullable=False)
    level = Column(String(DatabaseConstants.stock_frequency_len), nullable=False)

    def __repr__(self):
        return self.category_name

    @staticmethod
    def list_categories() -> list:
        session = get_database_session()
        return session.query(Category).all()

    @staticmethod
    def get_category_by_id(cat_id):
        session = get_database_session()
        return session.query(Category).filter_by(id=cat_id).first()

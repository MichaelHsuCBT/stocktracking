class DatabaseConstants:
    category_name_len = 64
    stock_item_name_len = 32
    supermarket_name_len = 128
    stock_unit_len = 8
    stock_frequency_len = 24
    location_len = 128
    COMPLETE = 'Completed'
    ONGOING = 'Ongoing'
    IMMEDIATELY = 'Restore Immediately'
    OFTEN = 'Often'
    OCCASIONALLY = 'Occasionally'

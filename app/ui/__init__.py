from flask_appbuilder import AppBuilder

from .categories import CategoryView
from .stock_items import StockItemGlobalView, StockItemRelatedView
from .supermarkets import SuperMarketView
from .group_category_stockitems import GroupCategoryStockItemView
from .shopping_plans import ShoppingPlansCompleteView, ShoppingPlanOngoingView, ShoppingPlanFormView


def register_ui_view(appbuilder: AppBuilder, db) -> None:
    """
    This function is responsible for informing the Appbuilder object of
    all of the views in the system, which allows AppBuilder to create and
    register all the URI paths and wire everything up.
    :param appbuilder: AppBuilder instance.
    :return: None
    """
    #######################################################
    # Create db
    #######################################################
    db.create_all()

    #######################################################
    # Top Level item Menu
    #######################################################
    appbuilder.add_view(GroupCategoryStockItemView,
                        "GroupCategoryStockItem",
                        label="Group Items",
                        icon="fa-object-group",
                        category="Stock Items",
                        category_icon="fa-shopping-cart"
                        )
    appbuilder.add_view(StockItemGlobalView,
                        "StockItem",
                        label="All Items",
                        icon="fa-shopping-basket",
                        category="Stock Items"
                        )
    appbuilder.add_view_no_menu(StockItemRelatedView)

    #######################################################
    # Top Level Setting Menu
    #######################################################

    appbuilder.add_view(CategoryView,
                        "Category",
                        label="Category",
                        icon="fa-list-alt",
                        category="Settings",
                        category_icon="fa-cogs"
                        )

    appbuilder.add_view(SuperMarketView,
                        "Supermarket",
                        label="Supermarkets",
                        icon="fa-shopping-cart",
                        category="Settings"
                        )

    #######################################################
    # Top Level Shopping Plan Menu
    #######################################################

    appbuilder.add_view(ShoppingPlansCompleteView,
                        "CompleteShoppingPlans",
                        label="Complete Shopping Plans",
                        icon="fa-history",
                        category="Shopping Plans",
                        category_icon="fa-list"
                        )
    appbuilder.add_view(ShoppingPlanOngoingView,
                        "OngoingShoppingPlan",
                        label="Ongoing Shopping Plan",
                        icon="fa-list-ol",
                        category="Shopping Plans",
                        category_icon="fa-list"
                        )
    appbuilder.add_view(ShoppingPlanFormView,
                        "CreateShoppingPlan",
                        label="Create a Shopping Plan",
                        icon="fa-plus",
                        category="Shopping Plans",
                        category_icon="fa-list"
                        )

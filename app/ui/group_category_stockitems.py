from flask_appbuilder import MasterDetailView
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder.widgets import ListWidget
from app.database import Category
from flask_appbuilder.security.decorators import has_access
from flask_appbuilder import expose
from flask_appbuilder.urltools import get_filter_args, get_order_args, get_page_args, get_page_size_args

from . import StockItemRelatedView


class ListMasterWidgetCatogoryItem(ListWidget):
    template = "/widgets/list_master.html"


class GroupCategoryStockItemView(MasterDetailView):

    datamodel = SQLAInterface(Category)
    related_views = [StockItemRelatedView]

    list_template = "custom_left_master_detail.html"
    list_widget = ListMasterWidgetCatogoryItem

    @expose("/list/")
    @expose("/list/<pk>")
    @has_access
    def list(self, pk=None):
        """
        Override this method to pass the pk to the render_template. This pk, which is the category ID, will be used
        to create a stock item.
        :param pk: the category id
        :return:
        """
        pages = get_page_args()
        page_sizes = get_page_size_args()
        orders = get_order_args()

        widgets = self._list()
        if pk:
            item = self.datamodel.get(pk)
            widgets = self._get_related_views_widgets(
                item, orders=orders, pages=pages, page_sizes=page_sizes, widgets=widgets
            )
            related_views = self._related_views
        else:
            related_views = []

        return self.render_template(
            self.list_template,
            title=self.list_title,
            widgets=widgets,
            related_views=related_views,
            master_div_width=self.master_div_width,
            pk=pk,
        )

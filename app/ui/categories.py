from flask_appbuilder import ModelView
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, Select2Widget
from flask_appbuilder.forms import DynamicForm
from flask_appbuilder.models.sqla.interface import SQLAInterface
from wtforms import StringField, SelectField
from wtforms.validators import DataRequired, Length

from app.database import Category, DatabaseConstants
from .validators import trim


class CategoryForm(DynamicForm):
    """
    Base class for the category form which includes fields and widgets,
    which are applicable for both adding and editing a Category entry
    """
    category_name = StringField(
        'Category Name',
        validators=[DataRequired(), Length(1, DatabaseConstants.category_name_len)],
        filters=[trim],
        widget=BS3TextFieldWidget(),
        description="The name of Category"
    )
    level = SelectField(
        'Level of Category',
        choices=[
            (DatabaseConstants.IMMEDIATELY, DatabaseConstants.IMMEDIATELY),
            (DatabaseConstants.OFTEN, DatabaseConstants.OFTEN),
            (DatabaseConstants.OCCASIONALLY, DatabaseConstants.OCCASIONALLY)
        ],
        widget=Select2Widget()
    )


class CategoryView(ModelView):
    datamodel = SQLAInterface(Category)

    add_form = CategoryForm
    edit_form = CategoryForm

    list_title = 'Available Categories'
    show_title = 'Category Detail'
    add_title = 'Add a new Category'
    edit_title = 'Edit the Category'

    add_columns = ['category_name', 'level']
    edit_columns = ['category_name', 'level']
    list_columns = ['category_name', 'level']

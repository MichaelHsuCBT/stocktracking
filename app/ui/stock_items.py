from flask import request, flash, redirect
from flask_appbuilder import ModelView
from flask_appbuilder import expose
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget, Select2Widget
from flask_appbuilder.forms import DynamicForm
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder.widgets import FormWidget, ListWidget, ShowBlockWidget
from wtforms import StringField, SelectField, IntegerField
from wtforms.validators import DataRequired, Length

from app.database import StockItem, DatabaseConstants, ShoppingPlan, Category
from app.database import add_object_to_database, commit_database_transaction
from .form_utils import create_category_choices
from .validators import trim
from .shopping_plans import ShoppingPlanOngoingView


class RelatedStockItemListWidget(ListWidget):
    template = "/widgets/related_stockitem_list_widget.html"


class StockItemForm(DynamicForm):
    item_name = StringField(
        'Item Name',
        validators=[DataRequired(), Length(1, DatabaseConstants.stock_item_name_len)],
        filters=[trim],
        widget=BS3TextFieldWidget(),
        description='Name of the item'
    )
    category_id = SelectField(
        'Category',
        coerce=int,
        widget=Select2Widget()
    )
    unit = StringField(
        'Unit',
        validators=[DataRequired(), Length(1, DatabaseConstants.stock_unit_len)],
        filters=[trim],
        widget=BS3TextFieldWidget(),
        description='Unit of the item'
    )
    percentage = IntegerField(
        'Percentage',
        widget=BS3TextFieldWidget(),
    )
    frequency = SelectField(
        'Frequency',
        widget=Select2Widget(),
        choices=[(DatabaseConstants.IMMEDIATELY, DatabaseConstants.IMMEDIATELY),
                 (DatabaseConstants.OFTEN, DatabaseConstants.OFTEN),
                 (DatabaseConstants.OCCASIONALLY, DatabaseConstants.OCCASIONALLY)]
    )

    def __init__(self, *args, **kwargs):
        super(StockItemForm, self).__init__(*args, **kwargs)
        self.category_id.choices = create_category_choices()


class StockItemBaseView(ModelView):
    datamodel = SQLAInterface(StockItem)

    related_views = [ShoppingPlanOngoingView]

    add_form = StockItemForm
    edit_form = StockItemForm

    list_title = 'All Items'
    show_title = 'Item Detail'
    add_title = 'Add a new Item'
    edit_title = 'Edit the Item'

    list_columns = ['id', 'item_name', 'category', 'unit', 'frequency', 'percentage']

    add_columns = ['item_name', 'category_id', 'unit', 'frequency', 'percentage']

    show_columns = ['id', 'item_name', 'category', 'unit', 'frequency', 'percentage']

    edit_columns = ['item_name', 'category_id', 'unit', 'frequency', 'percentage']

    show_template = 'appbuilder/general/model/show_cascade.html'

    def pre_update(self, item):
        # override this method to add item into m the ongoing JobPlan
        if (item.frequency == DatabaseConstants.IMMEDIATELY and item.percentage <= 20)\
                or (item.frequency == DatabaseConstants.OFTEN and item.percentage <= 5):
            ongoing_shopping_plan = ShoppingPlan.get_ongoing_shopping_plan()
            if ongoing_shopping_plan:
                if item not in ongoing_shopping_plan.stock_item_list:
                    ongoing_shopping_plan.stock_item_list.append(item)


class StockItemGlobalView(StockItemBaseView):
    list_title = 'All Stock Items'

    search_columns = ['item_name']

    list_widget = ListWidget


class StockItemRelatedView(StockItemBaseView):
    list_title = 'All Stock Items'

    include_columns = ['item_name', 'category_id', 'unit', 'frequency', 'percentage']

    order_columns = ['item_name']

    # Override the list_widget with related_stockitem_list_widget
    list_widget = RelatedStockItemListWidget

    def render(self, add_form: StockItemForm, title=list_title, include_columns=include_columns):
        """
        Define this render method to replace the render_template method.
        This render method will take the customized form as an input, and the form object can be accessed in both GET
        and POST request method.
        :param add_form: The StockItemForm object, which we define to host the stock item add form
        :param title: Title of the page
        :param include_columns: list containing which fields to show in the add form
        :return:
        """
        return self.render_template("appbuilder/general/model/add.html", title=title,
                                    widgets={'add': FormWidget(form=add_form, include_cols=include_columns)})

    @expose("/add_item/<int:category_id>", methods=['GET', 'POST'])
    def add_item(self, category_id):
        """
        Define the custom method to add a stock item from the category.
        This add_item replace the default add method because the default add won't take category id as an input.
        :param category_id: The category ID
        :return:
        """

        # initiate the add form
        add_form = StockItemForm()

        self.update_redirect()

        if request.method == 'GET':
            # initiate the category_id drop-down list with the category_id
            add_form.category_id.data = category_id
            add_form.frequency.data = Category.get_category_by_id(category_id).level

            return self.render(add_form)

        if not add_form.validate():
            return self.render(add_form)
        else:
            try:
                # get the data from form object, not from the request.form!
                item_name = add_form.item_name.data
                category_id = add_form.category_id.data
                unit = add_form.unit.data
                percentage = add_form.percentage.data
                frequency = add_form.frequency.data

                # Create the StockItem object with the initial values
                new_stock_item = StockItem(
                    item_name=item_name,
                    category_id=category_id,
                    unit=unit,
                    percentage=percentage,
                    frequency=frequency,
                )

                # Add the newly created object into the database
                add_object_to_database(new_stock_item)
                # Then commit the transaction
                commit_database_transaction()
                return redirect(self.get_redirect())
            except Exception as e:
                flash(f'Exception: {e}', 'danger')
                return self.render(add_form)

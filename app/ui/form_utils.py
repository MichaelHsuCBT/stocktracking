from app.database import Category, Supermarket


def create_category_choices() -> [tuple]:
    """
    This function will generate the choices for the selection field from the source list
    :return: a list of tuples
    """
    choices = []
    for item in Category.list_categories():
        choices.append((item.id, item.category_name))
    return choices


def create_supermarket_choices() -> [tuple]:
    """
    This function will generate the choices for the selection field from the source list
    :return: a list of tuples
    """
    choices = []
    for item in Supermarket.list_supermarkets():
        choices.append((item.id, item.supermarket_name))
    return choices

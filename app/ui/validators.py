from typing import Union


def trim(value: str) -> Union[str, None]:
    """
    Remoces the blank spaces from the beginning and end of the string
    :param value: String to be trimmed
    :return: Stripped String or None if Empty String
    """
    if not value:
        return None
    strip_string = value.strip()
    if not strip_string:
        return None
    return strip_string

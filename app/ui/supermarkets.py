from flask_appbuilder import ModelView
from flask_appbuilder.fieldwidgets import BS3TextFieldWidget
from flask_appbuilder.forms import DynamicForm
from flask_appbuilder.models.sqla.interface import SQLAInterface
from wtforms import StringField, IntegerField, DecimalField
from wtforms.validators import DataRequired, Length

from app.database import Supermarket, DatabaseConstants
from .validators import trim


class SuperMarketForm(DynamicForm):
    """
    Base class for the category form which includes fields and widgets,
    which are applicable for both adding and editing a Category entry
    """
    supermarket_name = StringField(
        'Supermarket Name',
        validators=[DataRequired(), Length(1, DatabaseConstants.supermarket_name_len)],
        filters=[trim],
        widget=BS3TextFieldWidget(),
        description="The name of Supermarket"
    )
    location = StringField(
        'Location',
        validators=[DataRequired(), Length(1, DatabaseConstants.location_len)],
        filters=[trim],
        widget=BS3TextFieldWidget(),
        description="Location of the Supermarket"
    )
    distance = DecimalField(
        'Distance',
        widget=BS3TextFieldWidget()
    )
    total_visits = IntegerField(
        'Total Visits',
        default=0,
        widget=BS3TextFieldWidget()
    )


class SuperMarketView(ModelView):
    datamodel = SQLAInterface(Supermarket)

    add_form = SuperMarketForm

    list_title = 'Supermarkets'
    show_title = 'Supermarkets Detail'
    add_title = 'Add a new Supermarkets'
    edit_title = 'Edit the Supermarkets'

    add_columns = ['supermarket_name', 'location', 'distance', 'total_visits']
    list_columns = ['supermarket_name', 'location', 'distance']
    edit_columns = ['supermarket_name', 'location', 'distance', 'total_visits']

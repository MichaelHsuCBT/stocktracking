from datetime import date

from flask import url_for, redirect, flash, request
from flask_appbuilder import ModelView, SimpleFormView
from flask_appbuilder import action, expose, has_access
from flask_appbuilder.fieldwidgets import Select2Widget, DatePickerWidget
from flask_appbuilder.forms import DynamicForm
from flask_appbuilder.models.sqla.filters import FilterEqual, FilterNotEqual
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder.widgets import ShowWidget
from sqlalchemy.orm.exc import MultipleResultsFound
from wtforms import SelectField, DateField
from operator import attrgetter

from app.database import ShoppingPlan, DatabaseConstants, StockItem, Category
from app.database import commit_database_transaction, add_object_to_database
from .form_utils import create_supermarket_choices

import traceback


class OngoingShoppingPlanWidget(ShowWidget):
    template = '/widgets/ongoing_shopping_plan_show_widget.html'


def remove_items_from_shopping_list(shopping_plan_id, item_id_list) -> None:
    """
    This function will update an ongoing shopping plan's shopping list by removing items, whose ids are given in the
    'item_id_list'.
    :param shopping_plan_id: The id of ongoing shopping plan
    :param item_id_list: the list of stock item ids, which will be removed from shopping list
    :return:
    """
    shopping_plan = ShoppingPlan.get_shopping_plan_by_id(shopping_plan_id)
    updated_shopping_list = []
    int_item_id_list = [int(item_id) for item_id in item_id_list]
    for stock_item in shopping_plan.stock_item_list:
        if stock_item.id not in int_item_id_list:
            updated_shopping_list.append(stock_item)
    shopping_plan.stock_item_list = updated_shopping_list
    commit_database_transaction()


def add_items_to_shopping_list(shopping_plan_id, item_id_list) -> None:
    """
    This function will update an ongoing shopping plan's shopping list by adding items.
    :param shopping_plan_id: The id of ongoing shopping plan
    :param item_id_list: The list of stock items ids, which will be added to the shopping plan
    :return:
    """
    shopping_plan = ShoppingPlan.get_shopping_plan_by_id(shopping_plan_id)
    int_item_id_list = [int(item_id) for item_id in item_id_list]
    items_to_add = StockItem.get_multiple_items_by_id_list(int_item_id_list)
    shopping_plan.stock_item_list = list(set(shopping_plan.stock_item_list).union(items_to_add))
    commit_database_transaction()


class ShoppingPlanForm(DynamicForm):
    supermarket_id = SelectField(
        'Supermarket',
        coerce=int,
        widget=Select2Widget()
    )
    plan_shopping_date = DateField(
        'Plan Shopping Date',
        widget=DatePickerWidget()
    )

    def __init__(self, *args, **kwargs):
        super(ShoppingPlanForm, self).__init__(*args, **kwargs)
        self.supermarket_id.choices = create_supermarket_choices()


class ShoppingPlanFormView(SimpleFormView):
    form = ShoppingPlanForm
    form_title = 'Create a new Shopping Plan'

    @expose("/form", methods=["GET"])
    @has_access
    def this_form_get(self):
        try:
            ongoing_shoppingplan = ShoppingPlan.get_ongoing_shopping_plan()
            if ongoing_shoppingplan:
                flash('Please update the existing Shopping Plan', 'danger')
                return redirect(url_for('ShoppingPlanOngoingView.list'))
        except MultipleResultsFound as e:
            flash(e, 'danger')
            return redirect(url_for('ShoppingPlanOngoingView.list'))
        self._init_vars()
        form = self.form.refresh()

        self.form_get(form)
        widgets = self._get_edit_widget(form=form)
        self.update_redirect()
        return self.render_template(
            self.form_template,
            title=self.form_title,
            widgets=widgets,
            appbuilder=self.appbuilder,
        )

    def form_get(self, form):
        form.plan_shopping_date.data = date.today()

    def form_post(self, form):
        flash(f'Form get created', 'success')
        supermarket_id = form.supermarket_id.data
        plan_shopping_date = form.plan_shopping_date.data
        status = DatabaseConstants.ONGOING

        # Create the ShoppingPlan object
        new_shopping_plan = ShoppingPlan(
            supermarket_id=supermarket_id,
            plan_shopping_date=plan_shopping_date,
            status=status
        )
        new_shopping_plan.stock_item_list = StockItem.get_all_lower_stock_items()

        # Add the new-created shopping_plan object into database
        add_object_to_database(new_shopping_plan)
        # Then commit the transaction
        commit_database_transaction()
        return redirect(url_for('ShoppingPlanOngoingView.show', pk=new_shopping_plan.id))


class ShoppingPlanBaseView(ModelView):

    datamodel = SQLAInterface(ShoppingPlan)

    add_title = "Add a new Shopping Plan"
    edit_title = "Edit a Shopping Plan"

    list_columns = ['id', 'supermarket', 'status', 'total_cost']

    show_columns = ['id', 'plan_shopping_date', 'plan_completion_date', 'supermarket', 'status', 'total_cost']

    item_list_columns = ['id', 'item_name', 'category_name', 'unit', 'frequency', 'percentage']
    item_label = {
        'id': 'ID',
        'item_name': 'Name',
        'category_name': 'Category',
        'unit': 'Unit',
        'frequency': 'Frequency',
        'percentage': 'Percentage',
    }


class ShoppingPlansCompleteView(ShoppingPlanBaseView):

    list_title = "Completed Shopping Plans"
    show_title = "Show Completed Shopping Plan"

    show_template = "shopping_plan_show.html"

    base_filters = [
        ['status', FilterEqual, 'Completed'],
    ]

    @expose("/show/<pk>", methods=["GET"])
    @has_access
    def show(self, pk):
        pk = self._deserialize_pk_if_composite(pk)
        widgets = self._show(pk)
        shopping_plan = self.datamodel.get(pk)
        stock_items = sorted(shopping_plan.stock_item_list, key=attrgetter('category_name', 'item_name'))
        return self.render_template(
            self.show_template,
            pk=pk,
            title=self.show_title,
            widgets=widgets,
            related_views=self._related_views,
            stock_items=stock_items,
            item_list_columns=self.item_list_columns,
            item_label=self.item_label,
        )


class ShoppingPlanOngoingView(ShoppingPlanBaseView):

    list_title = "Ongoing Shopping Plan"
    show_title = "Show Ongoing Shopping Plan"

    show_template = "ongoing_shopping_plan_show.html"
    show_widget = OngoingShoppingPlanWidget

    base_filters = [
        ['status', FilterNotEqual, 'Completed'],
    ]

    @expose("/show/<pk>", methods=["GET"])
    @has_access
    def show(self, pk):
        pk = self._deserialize_pk_if_composite(pk)
        widgets = self._show(pk)
        shopping_plan = self.datamodel.get(pk)
        shopping_list = sorted(shopping_plan.stock_item_list, key=attrgetter('category_name', 'item_name'))
        inventory_set = set(StockItem.get_all_stock_items()) - set(shopping_plan.stock_item_list)
        inventory_list = sorted(list(inventory_set), key=attrgetter('category_name', 'item_name'))
        category_list = sorted(Category.list_categories(), key=attrgetter('category_name'))
        return self.render_template(
            self.show_template,
            pk=pk,
            title=self.show_title,
            widgets=widgets,
            related_views=self._related_views,
            shopping_list=shopping_list,
            inventory_list=inventory_list,
            category_list=category_list,
            item_list_columns=self.item_list_columns,
            item_label=self.item_label,
        )

    @action('c_ShoppingPlanComplete', 'Complete Shopping Plan', confirmation='', icon='fa-check', multiple=False)
    def complete_shopping_plan(self, entry):
        """
        Update the shopping plan status to Completed
        And add 100 to all the items' percentage in shopping list
        Then return to the Completed Shopping Plan list view
        :param entry: The access to Shopping Plan Object
        :return: The Completed Shopping Plan list view
        """
        entry.status = DatabaseConstants.COMPLETE
        entry.plan_completion_date = date.today()

        for item in entry.stock_item_list:
            item.percentage += 100

        commit_database_transaction()

        self.update_redirect()
        return redirect(url_for('ShoppingPlansCompleteView.list'))

    @action('a_ShoppingListUpdate', 'Update Shopping List', confirmation='', icon='fa-wrench', multiple=False)
    def update_shopping_list(self, entry):
        """
        Update the Shopping List of the shopping plan with the latest lower inventory
        The update function will only add items into the shopping list, if the low-percentage item is not in it
        After the shopping plan list gets updated, then return to the Shopping Plan object's Show View
        :param entry: The access to Shopping Plan Object
        :return: The current Shopping Plan object show view
        """
        current_list_set = set(entry.stock_item_list)
        target_list_set = set(StockItem.get_all_lower_stock_items())

        diff_set = target_list_set - current_list_set
        if diff_set:
            entry.stock_item_list += list(diff_set)
            commit_database_transaction()
            flash('The Shopping List has been updated', 'success')
        else:
            flash('The Shopping List is up-to-date', 'info')

        return redirect(url_for('ShoppingPlanOngoingView.show', pk=entry.id))

    @action('b_ShoppingListRegenerate', 'Regenerate Shopping List', confirmation='', icon='fa-repeat', multiple=False)
    def regenerate_shopping_list(self, entry):
        """
        This method will generate a list of all the current low-percentage items,
        and replace the Shopping Plan's list with the new list
        :param entry: The access to Shopping Plan Object
        :return: The current Shopping Plan object show view
        """
        target_list = StockItem.get_all_lower_stock_items()

        if set(entry.stock_item_list) != set(target_list):
            entry.stock_item_list += target_list
            commit_database_transaction()
            flash('Regenerate the Shopping List', 'success')
        else:
            flash('The Shopping List is up-to-date', 'info')

        return redirect(url_for('ShoppingPlanOngoingView.show', pk=entry.id))

    @expose('/remove_items/<int:shoppingplan_id>', methods=['POST'])
    @has_access
    def remove_items(self, shoppingplan_id):
        if request.form.get('ShoppingList_buffer') == '':
            flash('No items to be removed!', 'info')
            self.update_redirect()
            return redirect(self.get_redirect())
        stock_item_id_list = request.form.get('ShoppingList_buffer').split(',')
        try:
            remove_items_from_shopping_list(shoppingplan_id, stock_item_id_list)
            msg = ('Shopping List has been updated!', 'success')
        except Exception:
            msg = (f'Error Updating Shopping Plan! Error Message: {traceback.format_exc()}', 'error')
        finally:
            flash(*msg)
            self.update_redirect()
            return redirect(self.get_redirect())

    @expose('/add_items/<int:shoppingplan_id>', methods=['POST'])
    @has_access
    def add_items(self, shoppingplan_id):
        if request.form.get('Inventory_buffer') == '':
            flash('No Items to be added!', 'info')
            self.update_redirect()
            return redirect(self.get_redirect())
        stock_item_id_list = request.form.get('Inventory_buffer').split(',')
        try:
            add_items_to_shopping_list(shoppingplan_id, stock_item_id_list)
            msg = ('Shopping List has been updated!', 'success')
        except Exception:
            msg = (f'Error Updating Shopping Plan! Error Message: {traceback.format_exc()}', 'error')
        finally:
            flash(*msg)
            self.update_redirect()
            return redirect(self.get_redirect())

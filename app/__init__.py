import logging

from flask import Flask
from flask_appbuilder import AppBuilder, SQLA
from flask_migrate import Migrate

from .template_filters import PageTemplateFilter

"""
 Logging configuration
"""

logging.basicConfig(format="%(asctime)s:%(levelname)s:%(name)s:%(message)s")
logging.getLogger().setLevel(logging.DEBUG)

"""
Initialize Flask, Database, and Appbuilder
"""
appbuilder = None
auth_manager = None
migrate = None
db = None


def create_app():
    # Allow these variables to be modifies within this function context
    global appbuilder
    global auth_manager
    global migrate
    global db

    # Initialize Flask
    app = Flask(__name__)
    app.config.from_object("config")

    # Initialize Database
    db = SQLA(app)
    migrate = Migrate(app, db)

    # initialize AppBuilder
    appbuilder = AppBuilder(app, db.session)

    appbuilder.template_filters = PageTemplateFilter(appbuilder.get_app, appbuilder.sm)

    # Add views
    from .ui import register_ui_view
    register_ui_view(appbuilder, db)

    return app


"""
from sqlalchemy.engine import Engine
from sqlalchemy import event

#Only include this for SQLLite constraints
@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    # Will force sqllite contraint foreign keys
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
"""


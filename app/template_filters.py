from flask_appbuilder.filters import TemplateFilters, app_template_filter
from urllib.parse import quote_plus, urlparse, parse_qs, urlunparse, unquote, parse_qsl, urlencode
from flask import url_for, redirect, flash, request
from app.database.category import Category


class PageTemplateFilter(TemplateFilters):
    """
    This class inherits from the default class TemplateFilters.
    Methods within this class can either override the existing filter
    or can add new filters into the jinja2 template

    The 'app_template_filter' decorator turns the function into filter which can be used in template as
    {{ value|filter }}

    This class can be applied to application by:
    appbuilder.template_filters = PageTemplateFilter(appbuilder.get_app, appbuilder.sm)

    This class can be separated into another file if more filters will be added
    """

    @app_template_filter("add_filter")
    def add_filter(self, current_url: str, filter_name: str, filter_value: str) -> str:
        """
        This filter will add a filter to the current url
        :param current_url: string of the current url
        :param filter_name: the name of filer
        :param filter_value: the value of the filter
        :return: updated url with filter
        """
        # the urlparse function will return a ParseResult object like:
        # ParseResult(schema='', netloc='', path='', params='', query='', fragment='')
        parsed_url = urlparse(current_url)

        # the parse_qsl() function returns a list of tuples of (key, value) pair
        # and value is the parameter value
        url_parameters = dict(parse_qsl(parsed_url.query))

        if filter_name in url_parameters:
            url_parameters[filter_name] += f'|{filter_value}'
        else:
            url_parameters[filter_name] = f'{filter_value}'

        new_filter_string = urlencode(url_parameters)
        parsed_url_list = list(parsed_url)
        parsed_url_list[4] = new_filter_string

        return urlunparse(parsed_url_list)

    @app_template_filter("remove_filter")
    def remove_filter(self, current_url: str, filter_name: str, filter_value: str) -> str:
        """
        This filter will add a filter to the current url
        :param current_url: string of the current url
        :param filter_name: the name of filer
        :param filter_value: the value of the filter
        :return: updated url removing filter
        """
        parsed_url = urlparse(current_url)
        url_parameters = dict(parse_qsl(parsed_url.query))

        if filter_name not in url_parameters:
            return current_url
        current_filters = url_parameters[filter_name].split('|')
        updated_filters = list(set(current_filters) - {filter_value})
        updated_filter_value = '|'.join(updated_filters)
        if not updated_filter_value:
            del(url_parameters[filter_name])
        else:
            url_parameters[filter_name] = f'{updated_filter_value}'

        new_filter_string = urlencode(url_parameters)
        parsed_url_list = list(parsed_url)
        parsed_url_list[4] = new_filter_string

        return urlunparse(parsed_url_list)

    @app_template_filter("update_filter")
    def update_filter(self, current_url: str, filter_name: str, filter_value: str) -> str:
        """
        This filter will update a filter to the current url
        :param current_url: string of the current url
        :param filter_name: the name of filer
        :param filter_value: the value of the filter
        :return: updated url with filter
        """
        # the urlparse function will return a ParseResult object like:
        # ParseResult(schema='', netloc='', path='', params='', query='', fragment='')
        parsed_url = urlparse(current_url)

        # the parse_qsl() function returns a list of tuples of (key, value) pair
        # and value is the parameter value
        url_parameters = dict(parse_qsl(parsed_url.query))
        url_parameters[filter_name] = f'{filter_value}'

        new_filter_string = urlencode(url_parameters)
        parsed_url_list = list(parsed_url)
        parsed_url_list[4] = new_filter_string

        return urlunparse(parsed_url_list)

    @app_template_filter("clear_filter")
    def clear_filter(self, current_url: str, filter_name: str) -> str:
        """
        This filter will clear a filter in the current url
        :param current_url: string of the current url
        :param filter_name: the name of filer
        :return: updated url with filter
        """
        # the urlparse function will return a ParseResult object like:
        # ParseResult(schema='', netloc='', path='', params='', query='', fragment='')
        parsed_url = urlparse(current_url)

        # the parse_qsl() function returns a list of tuples of (key, value) pair
        # and value is the parameter value
        url_parameters = dict(parse_qsl(parsed_url.query))
        if filter_name not in url_parameters:
            return current_url

        del(url_parameters[filter_name])
        new_filter_string = urlencode(url_parameters)
        parsed_url_list = list(parsed_url)
        parsed_url_list[4] = new_filter_string

        return urlunparse(parsed_url_list)

    @app_template_filter("equal_on_single_category")
    def equal_on_single_category(self, input_list: list, value: str) -> list:
        """
        This filter will filter out the list on category name with value
        :param input_list: The list which will be filtered
        :param value: the filter value
        :return: the filtered list
        """
        if not value:
            return input_list
        result = []
        for item in input_list:
            if item.category_name == value:
                result.append(item)
        return result

    @app_template_filter("include_item_name")
    def include_item_name(self, input_list: list, value: str) -> list:
        """
        This filter will return items containing the value
        :param input_list: The list which will be filtered
        :param value: filter value
        :return: the filtered list
        """
        if not value:
            return input_list
        result = []
        test_value = value.lower()
        for item in input_list:
            if test_value in item.item_name.lower():
                result.append(item)
        return result

    @app_template_filter("get_category_name")
    def get_category_name(self, url_path: str) -> str:
        """
        this filter will get the category id from the url path, then return and retrieve the category name
        :param url_path: The current url path of the view, get from 'request.path'
        :return: category name
        """
        last_slash_index = url_path[::-1].index('/')
        category_id = int(url_path[-last_slash_index:])
        category = Category.get_category_by_id(category_id)
        return category.category_name or ''

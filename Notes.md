**How to do database migrate**

flask db init


**Every time when you make a db change**

flask db migrate
flask db upgrade

---
**How to access sqlite database**

*In the project folder*
sqlite3 app.db

show all tables:
.tables

query data in a table:
SELECT * FROM table;

delete queries from a table:
DELETE FROM table WHERE condition;

Quit a sqlite session:
.quit

---
**MYSQL:**
mysql -u username -p
password

show databases;

use database;

